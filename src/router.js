import Vue from 'vue'
import Router from 'vue-router'
//  import Login from './views/login/Login'
import Logout from './views/login/Logout'
//  import UserProfile from './views/dashboard/pages/UserProfile'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '',
      name: 'login',
      component: require('./views/login/Login.vue').default,
      meta: {
        requiresLogged: true,
      },
    },
    {
      path: '/logout',
      name: 'logout',
      component: Logout,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/dashboard',
      component: () => import('@/views/dashboard/Index'),
      children: [
        {
          name: '',
          path: 'nuevo-laboratorio',
          component: () => import('@/views/dashboard/pages/LaboratorioCreator'),
        },
        {
          name: 'nueva-actividad',
          path: 'nueva-actividad',
          props: true,
          component: () => import('@/components/textEditor/TextEditor'),
        },
        {
          name: 'view-laboratorio',
          path: 'view-laboratorio',
          props: true,
          component: () => import('@/views/laboratorios/ViewLaboratorio'),
        },
        // Dashboard
        {
          name: 'Laboratorios',
          path: '',
          component: () => import('@/views/dashboard/Dashboard'),
        },
        // Pages
        {
          name: 'Perfil',
          path: 'pages/user',
          component: require('./views/dashboard/pages/UserProfile.vue').default,
        },
        {
          name: 'Notificaciones',
          path: 'components/notifications',
          component: () => import('@/views/dashboard/component/Notifications'),
        },
        {
          name: 'Administracion de laboratorios',
          path: 'tables/regular-tables',
          component: () => import('@/views/dashboard/tables/RegularTables'),
        },
        {
          name: 'Icons',
          path: 'components/icons',
          component: () => import('@/views/dashboard/component/Icons'),
        },
        {
          name: 'Typography',
          path: 'components/typography',
          component: () => import('@/views/dashboard/component/Typography'),
        },
        // Tables
        // Maps
        {
          name: 'Google Maps',
          path: 'maps/google-maps',
          component: () => import('@/views/dashboard/maps/GoogleMaps'),
        },
      ],
    },
  ],
})
