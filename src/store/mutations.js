const mutations = {
    SET_BAR_IMAGE (state, payload) {
        state.barImage = payload
      },
    SET_DRAWER (state, payload) {
    state.drawer = payload
    },
    updateAccess (state, access) {
      state.accessToken = access
      },
      destroyToken (state) {
      state.accessToken = null
      state.refreshToken = null
      },
}

export default mutations
