const state = {
    barColor: 'rgba(0, 0, 0, .8), rgba(0, 0, 0, .8)',
    barImage: 'https://demos.creative-tim.com/material-dashboard/assets/img/sidebar-1.jpg',
    drawer: null,
    APIData: '', // received data from the backend API is stored here.
    accessToken: localStorage.getItem('access_token') || null, //   makes sure the user is logged in even after
    //  refreshing the page
    refreshToken: localStorage.getItem('refresh_token') || null,
}

export default state
