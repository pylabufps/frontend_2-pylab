import Vue from 'vue'
import Vuex from 'vuex'
import state from './state'
import getters from './getters'
import mutations from './mutations'
import actions from './actions'
import userProfileStore from './modules/UserProfileStore'
import loginStore from './modules/LoginStore'
import LabCreatorStore from './modules/LabCreatorStore'
import TextEditorStore from './modules/TextEditorStore'
Vue.use(Vuex)

export default new Vuex.Store({
  state,
  getters,
  mutations,
  actions,
  modules: {
    userProfileStore: userProfileStore,
    loginStore: loginStore,
    LabCreatorStore: LabCreatorStore,
    TextEditorStore: TextEditorStore,
  },
})
