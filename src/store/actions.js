import { axiosBase } from '../api/axios-base'
const actions = {
    refreshToken (context) {
        return new Promise((resolve, reject) => {
          axiosBase.post('/api/token/refresh/', {
            refresh: context.state.refreshToken,
          }) // send the stored refresh token to the backend API
            .then(response => { // if API sends back new access and refresh token update the store
              console.log('New access successfully generated')
              context.commit('updateAccess', response.data.access)
              resolve(response.data.access)
            })
            .catch(err => {
              console.log('error in refreshToken Task')
              reject(err) // error generating new access and refresh token because refresh token has expired
            })
        })
      },
    logoutUser (context) {
        if (context.getters.loggedIn) {
            return new Promise((resolve) => {
            axiosBase.post('/api/token/logout/')
                .then(() => {
                localStorage.removeItem('access_token')
                localStorage.removeItem('refresh_token')
                context.commit('destroyToken')
                })
                .catch(err => {
                localStorage.removeItem('access_token')
                localStorage.removeItem('refresh_token')
                context.commit('destroyToken')
                resolve(err)
                })
            })
        }
    },
}

export default actions
