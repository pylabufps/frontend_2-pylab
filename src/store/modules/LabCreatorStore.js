import { axiosBase } from '../../api/axios-base'
const state = {
    labEditando: '',
}
const getters = {
    getLabEditando: state => {
        return state.labEditando
    },
}
const mutations = {
    editarLab (state, idLaboratorio) {
        state.labEditando = idLaboratorio
    },
}
const actions = {
    getLaboratorios () {
        return new Promise((resolve, reject) => {
            axiosBase.get('api/laboratorios/laboratorios/')
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },
    getTipoLenguajes () {
        return new Promise((resolve, reject) => {
            axiosBase.get('api/laboratorios/tipo-lenguajes/')
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },
    agregarLaboratorio (context, credentials) {
        return new Promise((resolve, reject) => {
            axiosBase.post('api/laboratorios/laboratorios/', {
                nombre: credentials.nombreLaboratorio,
                id_autor: 5,
                id_tipo_lenguaje: credentials.lenguajeSeleccionado,
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },
}

export default {
state,
getters,
mutations,
actions,
}
