import { axiosBase } from '../../api/axios-base'
const state = {
}
const getters = {
}
const mutations = {
}
const actions = {
    actualizarPerfil (context, data) {
        return new Promise((resolve, reject) => {
            axiosBase.post('/app/profiles/registrar-usuario', {
            email: data.email,
            username: data.username,
            password: data.password,
            biografia: data.biografia,
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },
    getDescripcionUsuario (id) {
        return new Promise((resolve, reject) => {
            axiosBase.get('api/profiles/usuario/5')
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },
}

export default {
state,
getters,
mutations,
actions,
}
