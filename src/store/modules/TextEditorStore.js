import { axiosBase } from '../../api/axios-base'
const state = {
}
const getters = {
}
const mutations = {
}
const actions = {
    getTextActividad (context, credentials) {
        return new Promise((resolve, reject) => {
            axiosBase.get('api/laboratorios/actividad/1', {
                params: {
                    id_laboratorio: credentials.idLaboratorio,
                },
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },
    onEditorChange (context, credentials) {
        return new Promise((resolve, reject) => {
            axiosBase.put('api/laboratorios/actividad/1', {
                descripcion: credentials.text,
            })
            .then(response => {
                resolve(response)
            })
            .catch(error => {
                reject(error)
            })
        })
    },
}
export default {
state,
getters,
mutations,
actions,
}
