/* eslint-disable */
import 'codemirror/lib/codemirror.css'
import '@toast-ui/editor/dist/toastui-editor.css'
import 'highlight.js/styles/github.css';
import codeSyntaxHighlight from '@toast-ui/editor-plugin-code-syntax-highlight';
import { Editor } from '@toast-ui/vue-editor'
var TextEditorMixin = {
    data () {
        return {
          editorText: '',
        }
      },
    created: function () {
        this.getTextActividad()
    },
    components: {
        editor: Editor,
    },
    
    methods: {
        getTextActividad () {
          this.$store.dispatch('getTextActividad', {
            idLaboratorio: this.idLaboratorio
          })
          .then((response) => {
            this.editorText=response.data.descripcion
            this.$refs.toastuiEditor.invoke('setMarkdown', this.editorText)
          })
          .catch((err) => {
              console.log(err)
          })
        },
        onEditorChange () {
          const text = this.$refs.toastuiEditor.invoke('getMarkdown')
          if(text!='' && text!=null && text!='¶'){
            this.$store.dispatch('onEditorChange',{
              text: text,
            })
          }
        },
    },
         
}
 
export default TextEditorMixin