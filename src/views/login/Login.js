var loginMixin = {
    data () {
        return {
        signUp: false,
        username: '',
        contraseña: '',
        correo: '',
        wrongCred: false, // activates appropriate message if set to true
        registroExitoso: '',
        }
    },
    methods: {
        iniciarSesionUsuario () { // call loginUSer action
            this.$store.dispatch('iniciarSesionUsuario', {
                username: this.username,
                password: this.contraseña,
            })
                .then(() => {
                    this.wrongCred = false
                    this.$router.push({ name: 'Laboratorios' })
                })
                .catch(() => {
                    //  this.$notify({ title: 'Error', type: 'error', group: 'auth', text: 'Credenciales incorrectas' })
                    this.wrongCred = true // if the credentials were wrong set wrongCred to true
                })
        },
        registroUsario () {
        this.$store.dispatch('registroUsario', {
            username: this.username,
            email: this.correo,
            password: this.contraseña,
        })
            .then(() => {
                this.registroExitoso = true
                //  this.$notify({ title: 'Bienvenido a Pylab!', type: 'success', group: 'auth', text: 'Registro exitoso.' })
                this.signUp = !this.signUp
            })
            .catch((error) => {
                for (const i in error.response.data) {
                    console.log(i)
                    //  this.$notify({ title: 'Error', type:'error', group: 'auth', text: "\n "+error.response.data[i][0]+"\n "})
                }
                this.registroExitoso = false
            })
        },
    },
}
export default loginMixin
