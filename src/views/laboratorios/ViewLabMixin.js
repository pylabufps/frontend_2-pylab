/* eslint-disable */
import '@toast-ui/editor/dist/toastui-editor-viewer.css'
import { Viewer } from '@toast-ui/vue-editor'

var viewLabMixin = {
    data () {
        return {
          actividad: '',
          chosenFile: '',
        }
      },
    created: function () {
        this.getDescripcionActividad()
    },
    components: {
        viewer: Viewer
    },
    methods: {
        getDescripcionActividad () {
          this.$store.dispatch('getTextActividad', {
            idLaboratorio: this.idLaboratorio
          })
          .then((response) => {
            this.actividad = response.data
            console.log(this.editorText)
            this.$refs.toastuiEditor.invoke('setMarkdown', this.actividad.descripcion)
          })
          .catch((err) => {
              console.log(err)
          })
        },
    },
         
}
 
export default viewLabMixin