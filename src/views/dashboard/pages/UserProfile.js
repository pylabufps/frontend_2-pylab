var userProfileMixin = {
    data () {
        return {
        email: '',
        username: '',
        password: '',
        biografia: '',
        snackbar: false,
        }
    },
    mounted: function () {
        this.getDescripcionUsuario()
       },
    methods: {
        getDescripcionUsuario () {
            this.$store.dispatch('getDescripcionUsuario')
                 .then((response) => {
                    this.email = response.data.email
                    this.username = response.data.username
                    this.biografia = response.data.biografia
                 })
                 .catch((err) => {
                    console.log(err)
                 })
        },
        actualizarPerfil () {
            //  TODO: confirmar si desea cambiar estos valores (email, username) example
            this.$store.dispatch('actualizarPerfil', {
               email: this.email,
               username: this.username,
               password: this.password,
               biografia: this.biografia,
            })
                .then(() => {
                    this.snackbar = true
                })
                .catch(() => {
                    // TODO: Notificar del error
                })
        },
    },
}
export default userProfileMixin
