/* eslint-disable */
var labCreatorMixin = {
    data () {
        return{ 
        accion: null, 
        idLaboratorio: null, 
        observacionAccion: null,
        dialog3: false,
        dialogoAgregarLaboratorio: false,
        tipoLenguajes: [],
        lenguajeSeleccionado: '',
        nombreLaboratorio:'',
        laboratorios: [],
        }
    },
    created: function () {
        this.getTipoLenguajes(),
        this.getLaboratorios()
       },
    methods: {
        editarLab (idLaboratorio) {
            //this.$store.commit('editarLab', idLaboratorio)
            this.$router.push({name: 'nueva-actividad', params: {idLaboratorio: idLaboratorio}})
        },
        getLaboratorios () {
            this.$store.dispatch('getLaboratorios')
            .then((response) => {
                this.laboratorios=response.data
            })
            .catch((err) => {
               console.log(err)
            })
        },
        getTipoLenguajes () {
            this.$store.dispatch('getTipoLenguajes')
                 .then((response) => {
                    this.tipoLenguajes=response.data
                 })
                 .catch((err) => {
                    console.log(err)
                 })
        },
        agregarLaboratorio(){
            if(this.nombreLaboratorio=='' || this.nombreLaboratorio==null || this.lenguajeSeleccionado=='' || this.lenguajeSeleccionado==null){

            }else{
                this.$store.dispatch('agregarLaboratorio',{
                    nombreLaboratorio: this.nombreLaboratorio,
                    lenguajeSeleccionado: this.lenguajeSeleccionado.id_tipo_lenguaje,
                })
                .then((response) => {
                    this.getLaboratorios()
                })
                .catch((err) => {
                    console.log(err)
                })               
            }
        },
           
    }
};
 
export default labCreatorMixin