/* eslint-disable */
var principalPageMixin = {
    data () {
        return{
        laboratorios: [],
        }
    },
    created: function () {
        this.getLaboratorios()
       },
    methods: {
        redirectToLab (idLaboratorio) {
            console.log(idLaboratorio)
            this.$router.push({name:'view-laboratorio', params: {idLaboratorio: idLaboratorio}})
        },
        getLaboratorios () {
            this.$store.dispatch('getLaboratorios')
            .then((response) => {
                this.laboratorios=response.data
            })
            .catch((err) => {
               console.log(err)
            })
        },
    }
};
 
export default principalPageMixin